using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    Vector2 mousePos;
    public Camera cam;
    private void Update()
    {
        mousePos=cam.ScreenToWorldPoint(Input.mousePosition);

    }
    private void FixedUpdate()
    {
        transform.position = mousePos;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriod : MonoBehaviour
{
    public Sprite[] sprites;
    public float size = 0.8f;
    public float minSize = 0.3f;
    public float maxSize = 1.3f;
    public float speed = 500.0f;
    public float maxLifeTime = 50.0f;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D rb;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        _spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];

        this.transform.eulerAngles = new Vector3(0.0f, 0.0f, Random.value * 360.0f);
        this.transform.localScale = Vector3.one * this.size * 0.75f;

        rb.mass = this.size * 5;

    }

    public void SetTrajectory(Vector2 direction)
    {
        rb.AddForce(direction * this.speed);
        Destroy(this.gameObject, this.maxLifeTime);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Bullet")
    //    {
    //        if ((this.size * 0.5f) >= this.minSize * 0.75f)
    //        {
    //            CreateSplit();
    //            CreateSplit();
    //        }
    //        //FindObjectOfType<GameManager>().AsteroidDestroyed(this);
    //        Destroy(this.gameObject);

    //    }
    //}
    private void CreateSplit()
    {
        Vector2 position = this.transform.position;
        position += Random.insideUnitCircle * 0.75f;

        Asteriod half = Instantiate(this, position, this.transform.rotation);
        half.size = this.size * 0.5f;
        half.SetTrajectory(Random.insideUnitCircle.normalized * this.speed);
    }

}

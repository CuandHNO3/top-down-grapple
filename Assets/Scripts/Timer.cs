using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour
{
    bool timerActive = false;
    float currentTime;
    public int startMinutes;
    public Text currentTimeText;//显示数字

    void Start()
    {
        currentTime=startMinutes*60;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerActive)//开始倒计时
        {
            currentTime-=Time.deltaTime;
        }
        TimeSpan time=TimeSpan.FromSeconds(currentTime); 
        currentTimeText.text = currentTime.ToString();
    }

    public void StartTimer()
    {
        timerActive = true;
    }
    public void StopTimer()
    {
        timerActive=false;
    }
}

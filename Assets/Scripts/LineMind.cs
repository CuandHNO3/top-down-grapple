using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineMind : MonoBehaviour
{
    [SerializeField] private LineRenderer renderer;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetPosition(Vector2 pos)
    {
        if (!CanAppend(pos)) return;

        renderer.positionCount++;
        renderer.SetPosition(renderer.positionCount - 1, pos);
    }

    private bool CanAppend(Vector2 pos)
    {
        if (renderer.positionCount == 0) return true;
        return Vector2.Distance(renderer.GetPosition(renderer.positionCount - 1), pos) > LineManager.resolution;
    }
}

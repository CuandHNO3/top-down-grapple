using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;

    private Transform target;

    public float distance;
    public float MaxDistance=10f;
    public GameObject player;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        distance = Vector2.Distance(transform.position, target.position);
        if(distance<MaxDistance&&distance>0.8) //�жϽ�ɫ�ƶ���Χ
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // ��������߼�
            player.SetActive(false);
            // �ݻ���Ҷ���
            // TODO: ������Ϸ�����¼����������߼�
            FindObjectOfType<GameManager>().PlayerDied();

        }
    }
    
    
}

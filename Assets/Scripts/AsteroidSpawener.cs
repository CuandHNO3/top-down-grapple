using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawener : MonoBehaviour
{
    public Asteriod asteriodPrefab;
    public float trajectoryVariance = 37.0f;
    public float spawnRate = 2.0f;
    public float spawnDistance = 37.0f;
    public int spawnAmount = 1;

    void Start()
    {
        InvokeRepeating(nameof(Spawn), spawnRate, spawnRate);
    }

    private void Spawn()
    {
        for (int i = 0; i < spawnAmount; i++)
        {
            Vector3 spawnDirection = Random.insideUnitCircle.normalized * this.spawnDistance;
            Vector3 SpawnPoint = this.transform.position + spawnDirection;

            float variance = Random.Range(-this.trajectoryVariance, this.trajectoryVariance);
            Quaternion rotation = Quaternion.AngleAxis(variance, Vector3.forward);
            Asteriod asteriod = Instantiate(asteriodPrefab, SpawnPoint, rotation);
            asteriod.size = Random.Range(asteriod.minSize, asteriod.maxSize);
            asteriod.SetTrajectory(rotation * -spawnDirection);
        }
    }
}


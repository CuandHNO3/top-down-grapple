using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    //Rigidbody2D rb;
    //GrappleHook gh;

    [SerializeField] float speed = 0.5f;

    float mx;
    float my;
    float distance;
    public float maxDistance=5f;
    public GameObject player;
    private void Start()
    {
        
    }

    private void Update()
    {
        mx = Input.GetAxisRaw("Horizontal");
        my = Input.GetAxisRaw("Vertical");
        distance = Vector2.Distance(player.transform.position, new Vector2(mx, my));
        


    }

    private void FixedUpdate()
    {
        if(distance < maxDistance) 
        {
            Vector2 target = new Vector2(transform.position.x+mx,transform.position.y+my);
            Vector2 grapplePos=Vector2.Lerp(transform.position,target, speed*Time.fixedDeltaTime);
            transform.position = new Vector2(mx, my).normalized * speed;
        }
       
    }
}

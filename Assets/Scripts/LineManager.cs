using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineManager : MonoBehaviour
{

    public GameObject player;
    public Camera cam;
    Vector2 lastpos;
    Vector2 curpos;

    [SerializeField] private LineMind linePrefabs;
    private LineMind currentLine;

    public const float resolution = 0.1f;
    public float speed;
    private void Start()
    {
    }
    private void Update()
    {
        curpos = player.transform.position;
        speed = (Vector3.Magnitude(curpos - lastpos) / Time.deltaTime);
        lastpos = curpos;
        if (Input.GetMouseButtonDown(0))
        {
            currentLine = Instantiate(linePrefabs, player.transform.position, Quaternion.identity);
        }
        if (speed > 0.1)
        {
            //currentLine.SetPosition(player.transform.position);

        }



    }
}

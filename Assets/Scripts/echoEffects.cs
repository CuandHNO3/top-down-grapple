using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class echoEffects : MonoBehaviour
{
    public float timeBtwSpawns;
    public float starttimeBtwSpawns;

    public GameObject echo;
    private void Update()
    {
        if (timeBtwSpawns <= 0)
        {
            Instantiate(echo,transform.position,Quaternion.identity);
        }
        else
        {
            timeBtwSpawns -= Time.deltaTime;
        }
    }
}

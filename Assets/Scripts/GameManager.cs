using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using static Cinemachine.DocumentationSortingAttribute;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public ParticleSystem expolsion;
    public Text liveText;
    public int lives = 1;
    public GameObject gameOverScreen;
    public float respawnInvulnerabilityTime = 3.0f;
    public float respawnTime = 3.0f;



    public void PlayerDied()
    {
        this.expolsion.transform.position = this.player.transform.position;
        this.expolsion.Play();
        this.lives--;
        liveText.text = lives.ToString();
        if (this.lives == 0)
        {
            GameOver();
        }
        else
        {
            Invoke(nameof(Respawn), respawnTime);
        }
    }

    private void GameOver()
    {
        this.lives = 3;
        gameOverScreen.SetActive(true);
    }

    private void Respawn()
    {
        this.player.transform.position = Vector3.zero;
        this.player.gameObject.SetActive(true);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ChangeScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
}

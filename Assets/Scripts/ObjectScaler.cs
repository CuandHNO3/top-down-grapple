//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ObjectScaler : MonoBehaviour
//{
//    public float duration = 1.0f; // 缩放动画持续时间
//    public float targetScale = 0.5f; // 目标缩放比例

//    private Vector3 originalScale; // 原始缩放比例

//    void Start()
//    {
//        originalScale = transform.localScale; // 记录原始缩放比例
//        StartCoroutine(ScaleOverTime()); // 启动缩放协程
//    }

//    IEnumerator ScaleOverTime()
//    {
//        while (true)
//        {
//            float startTime = Time.time; // 记录开始时间
//            float endTime = startTime + duration; // 计算结束时间

//            while (Time.time < endTime) // 动画进行中
//            {
//                float t = (Time.time - startTime) / duration; // 计算插值因子
//                float scale = Mathf.Lerp(originalScale.x, originalScale.x * targetScale, t); // 计算缩放比例
//                transform.localScale = new Vector3(scale, scale, 1); // 更新缩放比例
//                yield return null; // 等待一帧
//            }

//            // 缩小到目标大小后，反向缩放到原始大小
//            startTime = Time.time;
//            endTime = startTime + duration;
//            while (Time.time < endTime)
//            {
//                float t = (Time.time - startTime) / duration;
//                float scale = Mathf.Lerp(originalScale.x * targetScale, originalScale.x, t);
//                transform.localScale = new Vector3(scale, scale, 1);
//                yield return null;
//            }

//            transform.localScale = originalScale; // 恢复原始缩放比例

//        }
//    }

//}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScaler : MonoBehaviour
{
    public float scaleFactor = 0.5f; // 缩放比例因子
    public float animationTime = 2f; // 动画的持续时间
    public float delayTime = 1f; // 动画结束后的延迟时间
    void Start()
    {
        // 初始化变量
        transform.localScale = Vector3.one;
        StartCoroutine(StartScaleAnimation());
    }
    IEnumerator StartScaleAnimation()
    {
        while (true)
        {
            yield return StartCoroutine(ScaleDown());
            yield return new WaitForSeconds(delayTime);
            yield return StartCoroutine(ScaleUp());
            yield return new WaitForSeconds(delayTime);
        }
    }
    IEnumerator ScaleDown()
    {
        float currentTime = 0f;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = startScale * scaleFactor;

        while (currentTime < animationTime)
        {
            currentTime += Time.deltaTime;
            transform.localScale = Vector3.Lerp(startScale, endScale, currentTime / animationTime);
            yield return null;
        }
    }
    IEnumerator ScaleUp()
    {
        float currentTime = 0f;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = Vector3.one;

        while (currentTime < animationTime)
        {
            currentTime += Time.deltaTime;
            transform.localScale = Vector3.Lerp(startScale, endScale, currentTime / animationTime);
            yield return null;
        }
    }
}


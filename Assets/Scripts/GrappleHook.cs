using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MonoBehaviour
{
    LineRenderer line;

    [SerializeField] LayerMask grappableMask;//获取抓取点的物理图层
    [SerializeField] LayerMask grappableMask1;//获取抓取点的物理图层
    [SerializeField] float maxDistance = 10f;//最大发射距离
    [SerializeField] float grappleSpeed = 10f;//抓取后移动速度
    [SerializeField] float grappleShootSpeed = 20f;
    bool isClick = false;

    //bool isGrappling = false;
    [HideInInspector] public bool retracting = false;//使变量不显示在Inspector中，但进行序列化

    Vector2 target;

    private void Start()
    {
        line = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) )
        {
            isClick = true;
        }


    }

    private void FixedUpdate()
    {
        if (isClick )
        {
            StartGrapple();
            isClick = false;
           
        }

        if (retracting)//移动玩家
        {
            Vector2 grapplePos=Vector2.Lerp(transform.position, target, grappleSpeed*Time.fixedDeltaTime);
            
            transform.position = grapplePos;

            line.SetPosition(0, transform.position);
            if(Vector2.Distance(transform.position, target) < 0.001f)//停止移动的条件
            {
                retracting = false;
                //isGrappling=false;
                line.enabled = false;
            }
        }
    }

    private void StartGrapple()
    {
        // 确定发射方向
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, maxDistance, grappableMask);//确定抓取点坐标
        RaycastHit2D hit2 = Physics2D.Raycast(transform.position, direction, maxDistance, grappableMask1);//确定抓取点坐标

        if (hit.collider != null)
        {
            
            target = hit.transform.position;// +direction.normalized*0.5f;
            
            line.enabled = true;
            line.positionCount = 2;//有两个位置点

            StartCoroutine(Grapple());//延长位移时间
        }
        if(hit2.collider != null)
        {
            target = hit2.transform.position;
            line.enabled = true;
            line.positionCount = 2;//有两个位置点

            StartCoroutine(Grapple());//延长位移时间
        }
    }
   

    IEnumerator Grapple()
    {
        float t = 0f;
        float time = 2f;
        line.SetPosition(0,transform.position);
        line.SetPosition(1, transform.position);

        Vector2 newPos;
        for (; t < time; t += grappleShootSpeed * Time.fixedDeltaTime)//生成连续顺滑的时间//让线逐渐变长
        {
            newPos=Vector2.Lerp(transform.position,target, t/time);//t/time是为了获取实际的延迟时间
            line.SetPosition(0, newPos);
            line.SetPosition(1, newPos);
            yield return null;//结束Coroutine

        }
        line.SetPosition(01,target);
        retracting = true;
    }

    
}

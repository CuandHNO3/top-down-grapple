using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayerOnCollision : MonoBehaviour
{
    private int originalLayer; // 存储原始的Layer

    void Start()
    {
        originalLayer = gameObject.layer; // 获取当前对象的Layer
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collision detected");
        GameObject otherObject = collision.gameObject; // 获取碰撞的对象

        if (otherObject.CompareTag("Player")) // 检查碰撞的对象是否为"Player"
        {
            gameObject.layer = LayerMask.NameToLayer("NewLayer"); // 设置新的Layer
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Collision ended");
        GameObject otherObject = collision.gameObject; // 获取结束碰撞的对象

        if (otherObject.CompareTag("Player")) // 检查结束碰撞的对象是否为"Player"
        {
            gameObject.layer = originalLayer; // 设置回原始的Layer
        }
    }

    
}

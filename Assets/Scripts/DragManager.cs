using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour
{
    private Camera cam;
    [SerializeField] private Line linePrefabs;
    private Line currentLine;

    public const float resolution = 0.1f;

    private void Start()
    {
        cam = Camera.main;
    }
    private void Update()
    {
        Vector2 mousePos=cam.ScreenToWorldPoint(Input.mousePosition);
        if(Input.GetMouseButtonDown(0))
        {
            currentLine = Instantiate(linePrefabs, mousePos, Quaternion.identity);
        }
        if (Input.GetMouseButton(0))
        {
            currentLine.SetPosition(mousePos);
        }
    }

}
